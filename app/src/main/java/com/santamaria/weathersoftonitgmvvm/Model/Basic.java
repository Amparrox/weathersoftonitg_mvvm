package com.santamaria.weathersoftonitgmvvm.Model;

import android.databinding.ObservableArrayList;
import android.os.Parcel;
import android.os.Parcelable;

public class Basic implements Parcelable {

    private int count;
    private ObservableArrayList<City> list;

    public Basic(int count, ObservableArrayList<City> list) {
        this.count = count;
        this.list = list;
    }

    protected Basic(Parcel in) {
        count = in.readInt();
    }

    public static final Creator<Basic> CREATOR = new Creator<Basic>() {
        @Override
        public Basic createFromParcel(Parcel in) {
            return new Basic(in);
        }

        @Override
        public Basic[] newArray(int size) {
            return new Basic[size];
        }
    };

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ObservableArrayList<City> getList() {
        return list;
    }

    public void setList(ObservableArrayList<City> list) {
        this.list = list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(count);
        dest.writeTypedList(list);
    }
}
