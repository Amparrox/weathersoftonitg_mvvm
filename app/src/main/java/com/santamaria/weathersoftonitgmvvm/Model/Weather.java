package com.santamaria.weathersoftonitgmvvm.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by santamae on 11/17/2017.
 */

public class Weather implements Parcelable {

    private String description;

    public Weather(String description) {
        this.description = description;
    }

    protected Weather(Parcel in) {
        description = in.readString();
    }

    public static final Creator<Weather> CREATOR = new Creator<Weather>() {
        @Override
        public Weather createFromParcel(Parcel in) {
            return new Weather(in);
        }

        @Override
        public Weather[] newArray(int size) {
            return new Weather[size];
        }
    };

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(description);
    }
}
