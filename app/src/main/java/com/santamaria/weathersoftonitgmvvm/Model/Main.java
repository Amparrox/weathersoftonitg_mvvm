package com.santamaria.weathersoftonitgmvvm.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by santamae on 11/17/2017.
 */

public class Main implements Parcelable {

    private double temp;
    private int humidity;

    public Main(double temp, int humidity) {
        this.temp = temp;
        this.humidity = humidity;
    }

    protected Main(Parcel in) {
        temp = in.readDouble();
        humidity = in.readInt();
    }

    public static final Creator<Main> CREATOR = new Creator<Main>() {
        @Override
        public Main createFromParcel(Parcel in) {
            return new Main(in);
        }

        @Override
        public Main[] newArray(int size) {
            return new Main[size];
        }
    };

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(temp);
        parcel.writeInt(humidity);
    }
}
