package com.santamaria.weathersoftonitgmvvm.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedList;

/**
 * Created by santamae on 11/17/2017.
 */

public class City implements Parcelable {

    private String name;
    private Coord coord;
    private Main main;
    private Wind wind;
    private LinkedList<Weather> weather;

    public City(String name, Coord coord, Main main, Wind wind, LinkedList<Weather> weather) {
        this.name = name;
        this.coord = coord;
        this.main = main;
        this.wind = wind;
        this.weather = weather;
    }

    protected City(Parcel in) {
        name = in.readString();
        coord = in.readParcelable(Coord.class.getClassLoader());
        main = in.readParcelable(Main.class.getClassLoader());
        wind = in.readParcelable(Wind.class.getClassLoader());
    }

    public static final Creator<City> CREATOR = new Creator<City>() {
        @Override
        public City createFromParcel(Parcel in) {
            return new City(in);
        }

        @Override
        public City[] newArray(int size) {
            return new City[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public LinkedList<Weather> getWeather() {
        return weather;
    }

    public void setWeather(LinkedList<Weather> weather) {
        this.weather = weather;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeParcelable(coord, i);
        parcel.writeParcelable(main, i);
        parcel.writeParcelable(wind, i);
    }
}
