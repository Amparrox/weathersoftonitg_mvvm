package com.santamaria.weathersoftonitgmvvm.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by santamae on 11/17/2017.
 */

public class Wind implements Parcelable {

    private double speed;

    public Wind(double speed) {
        this.speed = speed;
    }

    protected Wind(Parcel in) {
        speed = in.readDouble();
    }

    public static final Creator<Wind> CREATOR = new Creator<Wind>() {
        @Override
        public Wind createFromParcel(Parcel in) {
            return new Wind(in);
        }

        @Override
        public Wind[] newArray(int size) {
            return new Wind[size];
        }
    };

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(speed);
    }
}
