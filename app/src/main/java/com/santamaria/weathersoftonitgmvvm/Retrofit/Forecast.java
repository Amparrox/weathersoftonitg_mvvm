package com.santamaria.weathersoftonitgmvvm.Retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Forecast {

    public static final String APPID_KEY = "72a682ac924c977a5899f95aa7077796";
    private static final String BASE_URL = "http://api.openweathermap.org";

    public static ForecastAPI doCallGetCities() {

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ForecastAPI.class);


    }
}
