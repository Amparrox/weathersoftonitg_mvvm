package com.santamaria.weathersoftonitgmvvm.Location;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.santamaria.weathersoftonitgmvvm.View.MainActivity;

public class LocationAPI implements LocationListener {

    private final MainActivity activity;

    public LocationAPI(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            activity.viewModel.getCitiesData(location.getLatitude(), location.getLongitude(), activity);
        }
    }

    public MainActivity getActivity(){
        return activity;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
