package com.santamaria.weathersoftonitgmvvm.ViewModel;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.santamaria.weathersoftonitgmvvm.Adapter.CityRecyclerAdapter;
import com.santamaria.weathersoftonitgmvvm.Model.City;
import com.santamaria.weathersoftonitgmvvm.R;

public class CityViewModel extends BaseObservable {

    public ObservableArrayList<City> cityList = new ObservableArrayList<>();
    private static CityRecyclerAdapter.OnItemClickListener onItemClickListener = null;

    public CityViewModel(CityRecyclerAdapter.OnItemClickListener onCitySelectedListener) {

        onItemClickListener = onCitySelectedListener;
    }


    @BindingAdapter("android:text")
    public static void setText(TextView view, double value) {

        view.setText(Double.toString(value) + "º C");

    }

    @BindingAdapter("bind:items")
    public static void setAdapter(RecyclerView view, ObservableArrayList<City> cityList) {

        if (onItemClickListener != null && cityList != null) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
            view.setLayoutManager(layoutManager);
            CityRecyclerAdapter adapter = new CityRecyclerAdapter(cityList, R.layout.city_item, onItemClickListener);
            view.setAdapter(adapter);
        }
    }
}
