package com.santamaria.weathersoftonitgmvvm.ViewModel;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.view.View;
import android.widget.LinearLayout;

public class DetailedViewModel extends BaseObservable {

    public ObservableField<String> cityName = new ObservableField<>();
    public ObservableField<String> weatherDescription = new ObservableField<>();
    public ObservableField<String> temperature = new ObservableField<>();
    public ObservableField<String> humidity = new ObservableField<>();
    public ObservableField<String> windSpeed = new ObservableField<>();
    public ObservableField<Integer> visibility = new ObservableField<>();

    public DetailedViewModel() {

        visibility.set(View.INVISIBLE);

    }

    @BindingAdapter("android:visibility")
    public static void setVisibility(LinearLayout linearLayout, Integer visibility) {
        if (visibility == View.VISIBLE)
            linearLayout.setVisibility(View.VISIBLE);
        else
            linearLayout.setVisibility(View.INVISIBLE);
    }

}
