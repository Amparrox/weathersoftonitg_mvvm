package com.santamaria.weathersoftonitgmvvm.ViewModel;

import android.content.DialogInterface;
import android.databinding.ObservableField;
import android.support.v7.app.AlertDialog;

import com.santamaria.weathersoftonitgmvvm.R;
import com.santamaria.weathersoftonitgmvvm.View.AboutActivity;


public class AboutViewModel {

    public ObservableField<String> currentVersion = new ObservableField<>();
    public ObservableField<String> supportedVersion = new ObservableField<>();

    public AboutViewModel(String supportedVersion, String currentVersion) {

        this.currentVersion.set(currentVersion);
        this.supportedVersion.set(supportedVersion);
    }

    public void createAlertCloseApp(final AboutActivity activity) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(R.string.close_title);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage(R.string.close_msg);

        builder.setPositiveButton(R.string.close_btn_positive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                activity.finishAffinity();
            }
        });

        builder.setNegativeButton(activity.getString(R.string.close_btn_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.create().show();
    }

}
