package com.santamaria.weathersoftonitgmvvm.ViewModel;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.BaseObservable;
import android.databinding.ObservableField;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.santamaria.weathersoftonitgmvvm.Fragment.CityFragment;
import com.santamaria.weathersoftonitgmvvm.Fragment.DetailedFragment;
import com.santamaria.weathersoftonitgmvvm.Location.LocationAPI;
import com.santamaria.weathersoftonitgmvvm.Model.Basic;
import com.santamaria.weathersoftonitgmvvm.Model.City;
import com.santamaria.weathersoftonitgmvvm.R;
import com.santamaria.weathersoftonitgmvvm.Retrofit.Forecast;
import com.santamaria.weathersoftonitgmvvm.View.MainActivity;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LOCATION_SERVICE;

public class MainActivityViewModel extends BaseObservable {

    private final String BASIC_DATA_KEY_ARGUMENT = "BASIC_DATA_KEY_ARGUMENT";
    private final String LAST_POSITION_SELECTED_KEY_ARGUMENT = "LAST_POSITION_SELECTED_KEY_ARGUMENT";
    public static final String CITY_LIST_KEY_ARGUMENT = "CITY_LIST_KEY_ARGUMENT";
    public static final String CITY_NAME_KEY_ARGUMENT = "CITY_NAME_KEY_ARGUMENT";
    public static final String WEATHER_DESCRIPTION_KEY_ARGUMENT = "WEATHER_DESCRIPTION_KEY_ARGUMENT";
    public static final String TEMPERATURE_KEY_ARGUMENT = "TEMPERATURE_KEY_ARGUMENT";
    public static final String HUMIDITY_KEY_ARGUMENT = "HUMIDITY_KEY_ARGUMENT";
    public static final String WIND_SPEED_KEY_ARGUMENT = "WIND_SPEED_KEY_ARGUMENT";

    public static final int GPS_PERMISSION = 100;

    private final String CITY_FRAGMENT_TAG = "CITY_FRAGMENT_TAG";
    private final String units = "metric";

    public ObservableField<Integer> visibility = new ObservableField<>();
    public ObservableField<Basic> basicData = new ObservableField<>();

    private LocationAPI myLocation;
    private LocationManager locationManager;

    private boolean isSingleFragment;
    private int lastPositionSelected;

    public MainActivityViewModel(boolean isSingleFragment) {
        visibility.set(View.GONE);
        this.isSingleFragment = isSingleFragment;
        lastPositionSelected = -1;
    }

    public void onCreate(Bundle savedInstanceState, DrawerLayout drawerLayout, FragmentManager fragmentManager) {

        boolean showDrawer = true;

        if (savedInstanceState != null) {
            basicData.set((Basic) savedInstanceState.getParcelable(BASIC_DATA_KEY_ARGUMENT));
            lastPositionSelected = savedInstanceState.getInt(LAST_POSITION_SELECTED_KEY_ARGUMENT);

            initFragment(fragmentManager);

            if (lastPositionSelected > 0 && !isSingleFragment) {
                onCitySelected(lastPositionSelected, fragmentManager);
            }

            showDrawer = false;
        }

        //Show the Navigation Drawer
        if (showDrawer) {
            drawerLayout.openDrawer(Gravity.START);
        }

    }

    public void onResume() {
        if (myLocation != null && myLocation.getActivity() != null && checkGPSPermission(myLocation.getActivity())) {
            if (locationManager != null && myLocation != null) {

                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                    if (basicData.get() == null) {
                        visibility.set(View.VISIBLE);
                        visibility.notifyChange();
                    }

                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1800000, 1000f, myLocation);
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1800000, 1000f, myLocation);
                }
            }
        }
    }

    public void onStop() {

        if (locationManager != null && myLocation != null) {
            locationManager.removeUpdates(myLocation);
        }

    }

    public void onSaveInstanceState(Bundle outState) {

        outState.putParcelable(BASIC_DATA_KEY_ARGUMENT, basicData.get());

        if (lastPositionSelected > 0) {
            outState.putInt(LAST_POSITION_SELECTED_KEY_ARGUMENT, lastPositionSelected);
        }
    }

    public void createAlertDialogSetting(final Activity activity) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(R.string.alert_dialog_title);
        builder.setIcon(R.drawable.ic_setting_alert_dialog_icon);
        builder.setMessage(R.string.alert_dialog_message);

        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(activity.getApplicationContext(), "Thanks for your support.", Toast.LENGTH_SHORT).show();
            }
        });

        builder.create().show();
    }

    public void createAlertCloseApp(final Activity activity) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(R.string.close_title);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage(R.string.close_msg);

        builder.setPositiveButton(R.string.close_btn_positive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                activity.finish();
            }
        });

        builder.setNegativeButton(activity.getString(R.string.close_btn_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.create().show();
    }

    public void getCurrentLocation(final MainActivity mainActivity) {

        if (checkGPSPermission(mainActivity)) {
            if (locationManager == null) {
                locationManager = (LocationManager) mainActivity.getSystemService(LOCATION_SERVICE);
            }

            if (myLocation == null) {
                myLocation = new LocationAPI(mainActivity);
            }

            if (locationManager != null && !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                showGPSAlertDialog(mainActivity);
            } else {

                visibility.set(View.VISIBLE);
                visibility.notifyChange();

                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1800000, 1000f, myLocation);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1800000, 1000f, myLocation);
            }
        }

    }

    private void showGPSAlertDialog(final Context context) {

        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(context.getString(R.string.gps_alert_title));
        alertDialog.setMessage(context.getString(R.string.gps_alert_msg));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(R.string.gps_alert_btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }

    public void getCitiesData(final double latitude, final double longitude, final MainActivity activity) {

        Forecast.doCallGetCities().getCities(latitude, longitude, 40, Forecast.APPID_KEY, units, getLanguage()).enqueue(new Callback<Basic>() {
            @Override
            public void onResponse(Call<Basic> call, Response<Basic> response) {

                if (response.isSuccessful()) {

                    basicData = new ObservableField<>(response.body());
                    visibility.set(View.GONE);
                    visibility.notifyChange();
                    initFragment(activity.getSupportFragmentManager());

                }
            }

            @Override
            public void onFailure(Call<Basic> call, Throwable t) {
                visibility.set(View.GONE);
                visibility.notifyChange();

                if (activity != null && activity.getCurrentFocus() != null) {
                    Snackbar snackbar = Snackbar.make(activity.getCurrentFocus(), R.string.call_failure, Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });

                    snackbar.show();
                }
            }
        });
    }

    private void initFragment(final FragmentManager fragmentManager) {

        if (basicData.get() != null && basicData.get().getList() != null) {
            if (isSingleFragment) {

                if (fragmentManager.findFragmentByTag(CITY_FRAGMENT_TAG) == null) {
                    CityFragment cityFragment = new CityFragment();

                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList(CITY_LIST_KEY_ARGUMENT, basicData.get().getList());

                    cityFragment.setArguments(bundle);

                    fragmentManager.beginTransaction().add(R.id.fragment_container, cityFragment, CITY_FRAGMENT_TAG).commitAllowingStateLoss();
                }
            } else {

                CityFragment cityFragment = (CityFragment) fragmentManager.findFragmentById(R.id.idFragmentCity);
                cityFragment.updateDataInCityFragment(basicData.get().getList());

            }
        }
    }

    private String getLanguage() {

        String lang = Locale.getDefault().getLanguage();

        if (lang.compareToIgnoreCase("es") != 0) {
            lang = "en";
        }

        return lang;
    }

    public void onCitySelected(final int position, final FragmentManager fragmentManager) {

        City city = basicData.get().getList().get(position);
        lastPositionSelected = position;

        if (city != null) {

            Bundle bundle = createBundleDataToDetailFragment(city);

            if (isSingleFragment) {
                //Implementation for the second fragment
                DetailedFragment detailedFragment = new DetailedFragment();

                detailedFragment.setArguments(bundle);

                fragmentManager.beginTransaction().replace(R.id.fragment_container, detailedFragment).addToBackStack(null).commit();
            } else {

                DetailedFragment detailedFragment = (DetailedFragment) fragmentManager.findFragmentById(R.id.idDetailedFragment);
                detailedFragment.updateDataInDetailedFragment(bundle);

            }
        }
    }

    private Bundle createBundleDataToDetailFragment(final City city) {

        Bundle bundle = new Bundle();
        bundle.putString(CITY_NAME_KEY_ARGUMENT, city.getName());

        if (!city.getWeather().isEmpty()) {
            bundle.putString(WEATHER_DESCRIPTION_KEY_ARGUMENT, city.getWeather().get(0).getDescription());
        }

        bundle.putDouble(TEMPERATURE_KEY_ARGUMENT, city.getMain().getTemp());
        bundle.putInt(HUMIDITY_KEY_ARGUMENT, city.getMain().getHumidity());
        bundle.putDouble(WIND_SPEED_KEY_ARGUMENT, city.getWind().getSpeed());

        return bundle;

    }

    public boolean checkGPSPermission(final MainActivity activity) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, GPS_PERMISSION);
                return false;
            }
        }

        return true;
    }

}

