package com.santamaria.weathersoftonitgmvvm.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.santamaria.weathersoftonitgmvvm.R;
import com.santamaria.weathersoftonitgmvvm.ViewModel.DetailedViewModel;
import com.santamaria.weathersoftonitgmvvm.ViewModel.MainActivityViewModel;
import com.santamaria.weathersoftonitgmvvm.databinding.FragmentDetailedBinding;

public class DetailedFragment extends Fragment {

    private FragmentDetailedBinding binding;
    private DetailedViewModel viewModel;

    public DetailedFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        viewModel = new DetailedViewModel();
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detailed, container, false);

        //Get Bundle information
        Bundle bundle = getArguments();

        if (bundle != null) {

            //set information on each TextView
            setDataToViewModel(bundle);

        } else {
            binding.setViewModel(viewModel);
        }

        return binding.getRoot();
    }

    public void updateDataInDetailedFragment(Bundle bundle) {

        setDataToViewModel(bundle);
    }

    private void setDataToViewModel(Bundle bundle) {

        viewModel.cityName.set(bundle.getString(MainActivityViewModel.CITY_NAME_KEY_ARGUMENT));
        viewModel.weatherDescription.set(bundle.getString(MainActivityViewModel.WEATHER_DESCRIPTION_KEY_ARGUMENT));
        viewModel.temperature.set(bundle.getDouble(MainActivityViewModel.TEMPERATURE_KEY_ARGUMENT) + "º C");
        viewModel.humidity.set(bundle.getInt(MainActivityViewModel.HUMIDITY_KEY_ARGUMENT) + " %");
        viewModel.windSpeed.set(bundle.getDouble(MainActivityViewModel.WIND_SPEED_KEY_ARGUMENT) + " m/s");

        binding.setViewModel(viewModel);
        binding.executePendingBindings();

        setFragmentVisible();

    }

    private void setFragmentVisible() {
        viewModel.visibility.set(View.VISIBLE);
        viewModel.visibility.notifyChange();
    }
}
