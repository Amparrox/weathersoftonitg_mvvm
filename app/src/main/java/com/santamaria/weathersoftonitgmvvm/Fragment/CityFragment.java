package com.santamaria.weathersoftonitgmvvm.Fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.santamaria.weathersoftonitgmvvm.Adapter.CityRecyclerAdapter;
import com.santamaria.weathersoftonitgmvvm.Model.City;
import com.santamaria.weathersoftonitgmvvm.R;
import com.santamaria.weathersoftonitgmvvm.ViewModel.CityViewModel;
import com.santamaria.weathersoftonitgmvvm.ViewModel.MainActivityViewModel;
import com.santamaria.weathersoftonitgmvvm.databinding.FragmentCityBinding;

public class CityFragment extends Fragment {

    OnCitySelectedListener mCallback;

    CityViewModel viewModel;
    FragmentCityBinding binding;

    public CityFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        viewModel = new CityViewModel(createOnCitySelectedListener());
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_city, container, false);

        if (getArguments() != null) {

            viewModel.cityList = (ObservableArrayList<City>) getArguments().get(MainActivityViewModel.CITY_LIST_KEY_ARGUMENT);

            //If not empty or null, do Actions.
            if (viewModel.cityList != null && !viewModel.cityList.isEmpty()) {

                binding.setViewModel(viewModel);

            }
        }

        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    public void updateDataInCityFragment(final ObservableArrayList<City> cityList) {

        if (cityList != null) {

            viewModel.cityList = cityList;
            binding.setViewModel(viewModel);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (OnCitySelectedListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    private CityRecyclerAdapter.OnItemClickListener createOnCitySelectedListener() {
        return new CityRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mCallback.onCitySelected(position);
            }
        };
    }

    public interface OnCitySelectedListener {
        void onCitySelected(int position);
    }


}
