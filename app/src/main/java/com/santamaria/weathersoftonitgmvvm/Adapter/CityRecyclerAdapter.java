package com.santamaria.weathersoftonitgmvvm.Adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.santamaria.weathersoftonitgmvvm.Model.City;
import com.santamaria.weathersoftonitgmvvm.databinding.CityItemBinding;

public class CityRecyclerAdapter extends RecyclerView.Adapter<CityRecyclerAdapter.ViewHolder> {

    private ObservableArrayList<City> cityList;
    private int layout;
    private OnItemClickListener listener;

    public CityRecyclerAdapter(ObservableArrayList<City> cityList, int layout, OnItemClickListener listener) {
        this.cityList = cityList;
        this.layout = layout;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(cityList.get(position), position);
    }

    @Override
    public int getItemCount() {

        int size = 0;

        if (cityList != null) {
            size = cityList.size();
        }

        return size;

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CityItemBinding binder;

        ViewHolder(View itemView) {
            super(itemView);
            binder = DataBindingUtil.bind(itemView);

        }

        void bind(final City city, final int position) {

            binder.setCity(city);
            binder.idCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(position);
                }
            });

            binder.executePendingBindings();

        }
    }

    //Interface to communicate with the Fragment about the item clicked in the RecyclerView
    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
