package com.santamaria.weathersoftonitgmvvm.View;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;

import com.santamaria.weathersoftonitgmvvm.Fragment.CityFragment;
import com.santamaria.weathersoftonitgmvvm.R;
import com.santamaria.weathersoftonitgmvvm.ViewModel.MainActivityViewModel;
import com.santamaria.weathersoftonitgmvvm.databinding.NavDrawerBinding;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, CityFragment.OnCitySelectedListener {

    private NavDrawerBinding binding;
    public MainActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.nav_drawer);
        viewModel = new MainActivityViewModel(isSingleFragment());

        binding.setViewModel(viewModel);
        binding.executePendingBindings();

        viewModel.onCreate(savedInstanceState, binding.idDrawerLayout, getSupportFragmentManager());

        //ActionBar
        setActionBar();

        binding.idNavDrawer.setNavigationItemSelectedListener(this);

    }

    //Enable the ActionBar
    private void setActionBar() {

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_nav_drawer);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_actionbar_items_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.idSetting:
                viewModel.createAlertDialogSetting(this);
                break;

            case R.id.idExit:
                viewModel.createAlertCloseApp(this);
                break;

            case android.R.id.home:
                binding.idDrawerLayout.openDrawer(Gravity.START);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        boolean closeNavDrawer = false;

        switch (item.getItemId()) {

            case R.id.idCities:

                if (viewModel.checkGPSPermission(this)) {
                    if (viewModel.basicData.get() == null) {
                        viewModel.getCurrentLocation(this);
                    }
                }

                closeNavDrawer = true;
                break;

            case R.id.idAbout:
                startAboutActivity();
                closeNavDrawer = true;
                break;

        }

        if (closeNavDrawer) {
            binding.idDrawerLayout.closeDrawer(Gravity.START);
        }

        return closeNavDrawer;
    }

    private void startAboutActivity() {

        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);

    }

    private boolean isSingleFragment() {
        return binding.fragmentContainer != null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        viewModel.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCitySelected(int position) {
        viewModel.onCitySelected(position, getSupportFragmentManager());
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getFragmentManager();

        if (binding.idDrawerLayout.isDrawerOpen(Gravity.START)) {
            binding.idDrawerLayout.closeDrawer(Gravity.START);
        } else if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        viewModel.onStop();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == MainActivityViewModel.GPS_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                viewModel.getCurrentLocation(this);
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
