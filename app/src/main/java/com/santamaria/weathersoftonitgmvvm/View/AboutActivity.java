package com.santamaria.weathersoftonitgmvvm.View;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.santamaria.weathersoftonitgmvvm.R;
import com.santamaria.weathersoftonitgmvvm.ViewModel.AboutViewModel;
import com.santamaria.weathersoftonitgmvvm.databinding.ActivityAboutBinding;

public class AboutActivity extends AppCompatActivity {


    AboutViewModel viewModel;
    ActivityAboutBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_about);

        viewModel = new AboutViewModel(getSupportedVersions(), getVersionInfo());

        binding.setViewModel(viewModel);

        setTitle("About");

    }

    private String getSupportedVersions() {

        String supportedVersions = getString(R.string.supported_versions);
        supportedVersions = " " + supportedVersions.replace("\\n", "\n");

        return supportedVersions;
    }

    //get the current version number and name
    private String getVersionInfo() {
        String version = "";
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return version;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.about_actionbar_items_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.idExit:
                viewModel.createAlertCloseApp(this);
                break;

        }

        return super.onOptionsItemSelected(item);
    }


}
